from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import joblib
import pandas as pd
from starlette.responses import Response

# Load the trained model
model = joblib.load('model.pkl')

# Define input data schema using Pydantic
class InputData(BaseModel):
    BodyFat: float
    Age: int
    Weight: float
    Height: float
    Chest: float
    Hip: float
    Thigh: float
    Ankle: float
    Biceps: float
    Forearm: float
    Wrist: float

# Initialize FastAPI app
app = FastAPI()

# Define CORS settings
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],  # Adjust this to your frontend URL
    allow_credentials=True,
    allow_methods=["POST", "OPTIONS"],  # Include OPTIONS method
    allow_headers=["*"],
)
# Define endpoint to predict muscle weight
@app.post("/predict/")
def predict_muscle_weight(data: InputData):
    try:
        # Convert input data to DataFrame
        input_data_dict = data.dict()
        input_data_df = pd.DataFrame([input_data_dict])
        
        # Make prediction
        prediction = model.predict(input_data_df)[0]
        
        # Return prediction
        return {prediction}
    except Exception as e:
        # If an error occurs, return an HTTP 500 error with the error message
        raise HTTPException(status_code=500, detail=str(e))
